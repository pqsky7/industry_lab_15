package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qpen546 on 8/05/2017.
 */
public class TreeModelAdapter implements TreeModel{
    private NestingShape _adaptee;
    private List<TreeModelListener> _treeModelListeners;

    public TreeModelAdapter(NestingShape _adaptee) {
        this._adaptee = _adaptee;
        _treeModelListeners = new ArrayList<TreeModelListener>();
    }

    @Override
    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;

        if (parent instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) parent;
            result = nestingShape.getChild(index);
        }
        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape shape = (Shape) parent;

        if(shape instanceof NestingShape){
            NestingShape nestingShape = (NestingShape) shape;
            result = nestingShape.shapeCount();
        }

        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if(parent instanceof NestingShape){
            NestingShape nestingShape = (NestingShape) parent;
            return nestingShape.indexOf((Shape)child);
        }
        return -1;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        _treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        _treeModelListeners.remove(l);
    }
}
