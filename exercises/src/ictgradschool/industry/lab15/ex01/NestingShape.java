package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;
import java.util.List;

/**
 * Created by qpen546 on 8/05/2017.
 */
public class NestingShape extends Shape {
    private List<Shape> _children;
    private NestingShape parent = null;

    public NestingShape() {
        _children = new ArrayList<Shape>();
    }

    public NestingShape(int x, int y) {
        super(x, y);
        _children = new ArrayList<Shape>();
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        _children = new ArrayList<Shape>();
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        _children = new ArrayList<Shape>();
    }

    public void add(Shape child) throws IllegalArgumentException {
        if(child.parent()!=null){
            throw new IllegalArgumentException();
        }
        if ( this.getWidth() <= child.getX()+child.getWidth() || this.getHeight() <= child.getY()+child.getHeight()) {
            System.out.print(this.getWidth());
            System.out.print(child.getX()+child.getWidth());
            throw new IllegalArgumentException();
        }

        if (_children.contains(child)){
            throw new IllegalArgumentException();
        }

        for (Shape _child : _children) {
            if (_child.getDeltaX() == child.getDeltaX() && _child.getDeltaY() == child.getDeltaY() && _child.getHeight() == child.getHeight() && _child.getWidth() == child.getWidth() && _child.getX() == child.getX() && _child.getY() == child.getY()) {
                throw new IllegalArgumentException();
            }
        }
        _children.add(child);
        child.setParent(this);
    }

    public void remove(Shape child) {
        if (_children.contains(child)) {
            _children.remove(child);
        }
        child.setParent(null);
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > _children.size() - 1) {
            throw new IndexOutOfBoundsException();
        }
        return _children.get(index);
    }

    public int shapeCount() {
        return _children.size();
    }

    public int indexOf(Shape child) {
        return _children.indexOf(child);
    }

    public boolean contains(Shape child) {
        return _children.contains(child);
    }

    public Shape getChild(int index){
        return _children.get(index);
    }

    @Override
    public void move(int width, int height) {
        super.move(width, height);
        for (Shape _child : _children) {
            _child.move(this.fWidth, this.fHeight);
        }
    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);
        for (Shape _child : _children) {
            _child.paint(painter);
        }
        painter.translate(-fX, -fY);
    }
}
